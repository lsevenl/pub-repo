﻿using System.Text.RegularExpressions;
using SevenShell.Core;

namespace SevenShell.Services
{
    /// <summary>
    /// Сервис для парса введеных в консоль данных( комманды и ее параметра), выбирает значение команды. 
    /// </summary>
    public class CommandParser : ICommandParser
    {
        private string[] _params = new []
        {
            "-Path",
            "-OldPath",
            "-NewPath",
            "-All",
            "CommandName"
        };
        /// <summary>
        /// Парсит строку и выбирает значение параметра команды. 
        /// </summary>
        public string ParsParameter(string inputString, string command)
        {
            var input = inputString.Remove(0, command.Length);
            var parameter = input.Remove(0, Regex.Match(input, @"-\S+").Value.Length);
            return parameter.Trim();
        }

        /// <summary>
        /// Парсит строку и выбирает имя команды.
        /// </summary>
        public string ParsCommand(string inputString)
        {
            var result = Regex.Match(inputString.ToLower(), @"[a-z]+-[a-z]+\s").Value;
            return result;
        }

        public string ParsCommand(string inputString, bool notOneParams)
        {
            var result = Regex.Match(inputString.ToLower(), @"([a-z]+-[a-z]+\s) ").Value;
            return result;
        }
    }
}
