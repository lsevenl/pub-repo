﻿using System;
using System.IO;
using System.Threading.Tasks;
using SevenShell.Core;

namespace SevenShell.Services
{
    /// <summary>
    /// Сервис для работы с директориями файловой системы.
    /// </summary>
    public class DirectoryService : IDirectoryService
    {
        public async Task CreateDirectoryAsync(string path)
        {
            await Task.Run(() =>
            {
                try
                {
                    if (!Directory.Exists(path))
                    {
                        var dir = Directory.CreateDirectory(path);
                        Console.WriteLine($"Директория успешно создана:\n Путь: {dir.FullName}");
                    }
                    else
                    {
                        Console.Beep();
                        Console.WriteLine("Директория уже существует");
                    }
                }
                catch(Exception ex)
                {
                    Console.Beep();
                    Console.WriteLine(ex.Message);
                }
            });
        }

        public async Task DeleteDirectoryAsync(string path)
        {
            await Task.Run(() =>
            {
                if (Directory.Exists(path))
                {
                    Directory.Delete(path);
                    Console.WriteLine("Операция выполнена успешно.");
                }
                else
                {
                    Console.Beep();
                    Console.WriteLine("Директории не существует");
                }
            });
        }

        public async Task MoveDirectoryAsync(string oldPath, string newPath)
        {
            await Task.Run(() =>
            {
                if (Directory.Exists(oldPath) && Directory.Exists(newPath))
                {
                    Directory.Move(oldPath, newPath);
                    Console.WriteLine("Операция выполнена успешно.");
                }
                else
                {
                    Console.Beep();
                    Console.WriteLine("Один из путей не найден");
                }
            });
        }

        //public async Task SearchDirectoryAsync(string Path)
        //{
        //    await Task.Run(() =>
        //    {
        //        if (Directory.Exists(oldPath) && Directory.Exists(newPath))
        //        {
        //            Directory.Move(oldPath, newPath);
        //            Console.WriteLine("Операция выполнена успешно.");
        //        }
        //        else
        //        {
        //            Console.Beep();
        //            Console.WriteLine("Один из путей не найден");
        //        }
        //    });
        //}
    }
}
