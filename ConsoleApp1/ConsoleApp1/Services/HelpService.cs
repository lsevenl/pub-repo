﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using SevenShell.Core;

namespace SevenShell.Services
{
    public class HelpService : IHelpService
    {
        /// <summary>
        /// Выводит описание команды и пример использования.
        /// </summary>
        public string GetCommandHelp(string commandName)
        {
            var commands = new Dictionary<string, string>
            {
                { "Add-Directory", "\nДобавляет новую директорию по указнному пути. Example: Add-Directory -Path c:\\Folder" },
                { "Remove-Directory", "\nУдаляет директорию по указаному пути. Example: Remove-Directory -Path c:\\Folder" },
                { "Move-Directory", "\nПеремещает директорию по новому пути. Example: Move-Directory -oldPath c:\\Folder -newPath d:\\" }
            };

            var result = commands.Keys.SingleOrDefault(x => x.ToLower().Equals(commandName.ToLower()));

            if (!string.IsNullOrEmpty(result))
            {
                commands.TryGetValue(result, out string value);
                return value;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Выводит полный список команд с описанием и примерами.
        /// </summary>
        public string GetHelpAll()
        {
            var commands = new StringBuilder()
                .Append("\n")
                .Append("\nAdd-Directory: Добавляет новую директорию по указнному пути.\n Example: Add-Directory -Path c:\\Folder\n")
                .Append("\nRemove-Directory: Удаляет директорию по указаному пути.\n Example: Remove-Directory -Path c:\\Folder\n")
                .Append("\nMove-Directory: Перемещает директорию по новому пути.\n Example: Move-Directory -oldPath c:\\Folder -newPath d:\\\n");

            return commands.ToString();
        }
    }
}
