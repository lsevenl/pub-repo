﻿using System.Threading.Tasks;
using Autofac;
using SevenShell.DI;

namespace SevenShell
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await IoC.GetContainer().Resolve<Application>().Run();
        }
    }
}
