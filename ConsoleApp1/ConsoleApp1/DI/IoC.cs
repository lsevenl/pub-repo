﻿using Autofac;
using SevenShell.Core;
using SevenShell.Services;

namespace SevenShell.DI
{
    public class IoC
    {
        /// <summary>
        /// Регистрация зависимостей , сопоставление классов с их интерфейсами для того чтоб использовать интерфейсы вместо создания экземпляров классов. 
        /// </summary>
        /// <returns></returns>
        public static IContainer GetContainer()
        {
            var container = new ContainerBuilder();
            container.RegisterType<Application>();
            container.RegisterType<DirectoryService>().As<IDirectoryService>();
            container.RegisterType<CommandParser>().As<ICommandParser>();
            container.RegisterType<HelpService>().As<IHelpService>();
            return container.Build();
        }
    }
}
