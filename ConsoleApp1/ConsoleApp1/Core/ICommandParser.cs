﻿namespace SevenShell.Core
{
    public interface ICommandParser
    {
        string ParsParameter(string inputString, string command);

        string ParsCommand(string inputString);
    }
}
