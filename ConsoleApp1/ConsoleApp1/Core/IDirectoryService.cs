﻿using System.Threading.Tasks;

namespace SevenShell.Core
{
    public interface IDirectoryService
    {
        Task CreateDirectoryAsync(string path);
        Task DeleteDirectoryAsync(string path);
        Task MoveDirectoryAsync(string oldPath, string newPath);
    }
}
