﻿namespace SevenShell.Core
{
    public interface IHelpService
    {
        string GetCommandHelp(string commandName);
        string GetHelpAll();
    }
}
