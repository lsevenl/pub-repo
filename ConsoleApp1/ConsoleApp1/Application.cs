﻿using System;
using System.Threading.Tasks;
using SevenShell.Core;

namespace SevenShell
{
    /// <summary>
    /// Приложение и вся его логика.
    /// </summary>
    public class Application
    {
        private readonly IDirectoryService _directoryService;
        private readonly ICommandParser _commandParser;
        private readonly IHelpService _helpService;
        public Application(IDirectoryService directoryService, ICommandParser commandParser, IHelpService helpService)
        {
            this._helpService = helpService;
            this._commandParser = commandParser;
            this._directoryService = directoryService;
        }

        public async Task Run()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Title = "Seven application";
            Console.WriteLine(@"
╔═══╗╔═══╗╔╗──╔╗╔═══╗╔═╗─╔╗     ╔═══╗╔╗─╔╗╔═══╗╔╗───╔╗───
║╔═╗║║╔══╝║╚╗╔╝║║╔══╝║║╚╗║║     ║╔═╗║║║─║║║╔══╝║║───║║───
║╚══╗║╚══╗╚╗║║╔╝║╚══╗║╔╗╚╝║     ║╚══╗║╚═╝║║╚══╗║║───║║───
╚══╗║║╔══╝─║╚╝║─║╔══╝║║╚╗║║     ╚══╗║║╔═╗║║╔══╝║║─╔╗║║─╔╗
║╚═╝║║╚══╗─╚╗╔╝─║╚══╗║║─║║║     ║╚═╝║║║─║║║╚══╗║╚═╝║║╚═╝║
╚═══╝╚═══╝──╚╝──╚═══╝╚╝─╚═╝     ╚═══╝╚╝─╚╝╚═══╝╚═══╝╚═══╝");
            Console.WriteLine("\nВведите команду или воспользуйтесь командами Get-Help -All, Get-CommandHelp -CommandName command");
            Console.WriteLine("\n");

            while (true)
            {
                var inputString = Console.ReadLine();
                var command = this._commandParser.ParsCommand(inputString);
                var paramValue = this._commandParser.ParsParameter(inputString, command);

                switch (command.Trim())
                {
                    case "add-directory": await this._directoryService.CreateDirectoryAsync(paramValue);
                        break;
                    case "remove-directory": await this._directoryService.DeleteDirectoryAsync(paramValue);
                        break;
                    case "move-directory": //await this._directoryService.MoveDirectoryAsync();
                        break;
                    case "get-help": Console.WriteLine(this._helpService.GetHelpAll());
                        break;
                    case "get-commandhelp": Console.WriteLine(this._helpService.GetCommandHelp(paramValue));
                        break;
                    default:
                        Console.Beep();
                        Console.WriteLine($"Команда {command} не распознана, воспользуйтесь командами для справки(Get-Help -All, Get-CommandHelp -CommandName command).");
                        break;
                }
            }
        }
    }
}
